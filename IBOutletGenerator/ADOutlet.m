//
//  ADIBOutlet.m
//  IBOutletGenerator
//
//                 LICENCE PUBLIQUE RIEN À BRANLER
// Version 1, Mars 2009
//
// Copyright (C) 2012 Jonathan Tremesaygues <jonathan@access-dev.com>
//
// La copie et la distribution de copies exactes de cette licence sont
// autorisées, et toute modification est permise à condition de changer
// le nom de la licence.
//
// CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION
// DE LA LICENCE PUBLIQUE RIEN À BRANLER
//
// 0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.

#import "ADOutlet.h"

@implementation ADOutlet

@synthesize type = _type;
@synthesize name = _name;
@dynamic isButton;
@dynamic isLabel;
@dynamic isScrollView;

- (id)init
{
    self = [self initWithType:@"" name:@""];
    
    return self;
}

- (id)initWithType:(NSString * const)type name:(NSString * const)name
{
    self = [super init];
    if (self != nil)
    {
        self.type = type;
        self.name = name;
    }
    
    return self;
}

+ (id)outletWithType:(NSString * const)type name:(NSString * const)name
{
    ADOutlet *outlet = [[ADOutlet alloc] initWithType:type name:name];
    
    return outlet;
}

- (BOOL)isButton
{
    const BOOL result = [_type isEqualToString:@"UIButton"];
    
    return result;
}

- (BOOL)isLabel
{
    const BOOL result = [_type isEqualToString:@"UILabel"];
    
    return result;
}

- (BOOL)isScrollView
{
    const BOOL result = [_type isEqualToString:@"UIScrollView"];
    
    return result;
}


@end
