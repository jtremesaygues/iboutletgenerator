//
//  main.m
//  IBOutletGenerator
//
//                 LICENCE PUBLIQUE RIEN À BRANLER
// Version 1, Mars 2009
//
// Copyright (C) 2012 Jonathan Tremesaygues <jonathan@access-dev.com>
//
// La copie et la distribution de copies exactes de cette licence sont
// autorisées, et toute modification est permise à condition de changer
// le nom de la licence.
//
// CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION
// DE LA LICENCE PUBLIQUE RIEN À BRANLER
//
// 0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
