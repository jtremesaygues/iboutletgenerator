//
//  ADAppDelegate.m
//  IBOutletGenerator
//
//                 LICENCE PUBLIQUE RIEN À BRANLER
// Version 1, Mars 2009
//
// Copyright (C) 2012 Jonathan Tremesaygues <jonathan@access-dev.com>
//
// La copie et la distribution de copies exactes de cette licence sont
// autorisées, et toute modification est permise à condition de changer
// le nom de la licence.
//
// CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION
// DE LA LICENCE PUBLIQUE RIEN À BRANLER
//
// 0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.

#import "ADAppDelegate.h"
#import "ADOutlet.h"

@implementation ADAppDelegate
@synthesize comboBoxCell = _comboBoxCell;
@synthesize tabView = _tabView;
@synthesize generateHTextView = _generateHTextView;
@synthesize generateMTextView = _generateMTextView;
@synthesize tableViewArrayController = _tableViewArrayController;


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Add values in comboxbox
    [_comboBoxCell addItemsWithObjectValues:[NSArray arrayWithObjects:
                                              @"UIButton",
                                              @"UILabel",
                                              @"UIScrollView",
                                              @"UITextField",
                                              @"UIView",
                                              nil]];
    
    // Always display .h tab on start up
    [_tabView selectTabViewItemAtIndex:0];
}

- (void)generateOutlets
{
    NSArray * const outlets = (NSArray *) _tableViewArrayController.arrangedObjects;
    
    [self generateHOutletsWithOutlets:outlets];
    [self generateMOutletsWithOutlets:outlets];
}

- (void)generateHOutletsWithOutlets:(NSArray * const)outlets
{
    // Generate .h
    NSMutableString * const outputHResult = [[NSMutableString alloc] init];
    
    // properties
    for (ADOutlet *outlet in outlets)
    {
        [outputHResult appendFormat:@"@property (nonatomic, retain) IBOutlet %@ *%@;\n", outlet.type, outlet.name];
    }
    [outputHResult appendString:@"\n"];
    
    
    // Actions
    for (ADOutlet *outlet in outlets)
    {
        if (outlet.isButton)
            [outputHResult appendFormat:@"- (IBAction)%@Pushed;\n", outlet.name];
    }
    [outputHResult appendString:@"\n"];
    _generateHTextView.string = outputHResult;

}

- (void)generateMOutletsWithOutlets:(NSArray * const)outlets
{
    // Generate .m
    NSMutableString * const outputMResult = [[NSMutableString alloc] init];;
    
    
    // Synthesize
    for (ADOutlet *outlet in outlets)
    {
        [outputMResult appendFormat:@"@synthesize %@ = _%@;\n", outlet.name, outlet.name];
    }
    [outputMResult appendString:@"\n"];
    
    
    // Dealloc
    [outputMResult appendString:@"- (void)dealloc\n"];
    [outputMResult appendString:@"{\n"];
    for (ADOutlet *outlet in outlets)
    {
        [outputMResult appendFormat:@"\t[_%@ release];\n", outlet.name];
    }
    [outputMResult appendString:@"\n"];
    [outputMResult appendString:@"\t[super dealloc];\n"];
    [outputMResult appendString:@"}\n"];
    [outputMResult appendString:@"\n"];
    
    
    // ViewDidLoad
    [outputMResult appendString:@"- (void)viewDidLoad\n"];
    [outputMResult appendString:@"{\n"];
    [outputMResult appendString:@"\t[super viewDidLoad];\n"];
    [outputMResult appendString:@"\n"];
    for (ADOutlet *outlet in outlets)
    {
        if (outlet.isLabel)
            [outputMResult appendFormat:@"\t_%@.text = @\"\";\n", outlet.name];
        else if (outlet.isButton)
            [outputMResult appendFormat:@"\t[_%@ setTitle:@\"\" forState:UIControlStateNormal];\n", outlet.name];
        else if (outlet.isScrollView)
            [outputMResult appendFormat:@"\t_%@.contentSize = CGSizeMake(0.0f, 0.0f);\n", outlet.name];
        else
            [outputMResult appendFormat:@"\t//_%@\n", outlet.name];
        [outputMResult appendString:@"\t\n"];
    }
    [outputMResult appendString:@"}\n"];
    [outputMResult appendString:@"\n"];
    
    
    
    
    // ViewDidUnload
    [outputMResult appendString:@"- (void)viewDidUnload\n"];
    [outputMResult appendString:@"{\n"];
    for (ADOutlet *iboutlet in outlets)
    {
        [outputMResult appendFormat:@"\tself.%@ = nil;\n", iboutlet.name];
    }
    [outputMResult appendString:@"\n"];
    [outputMResult appendString:@"\t[super viewDidUnload];\n"];
    [outputMResult appendString:@"}\n"];
    [outputMResult appendString:@"\n"];
    
    
    // Actions
    [outputMResult appendString:@"\n"];
    [outputMResult appendString:@"\n"];
    [outputMResult appendString:@"#pragma mark - Actions\n"];
    [outputMResult appendString:@"\n"];
    for (ADOutlet *iboutlet in outlets)
    {
        if (!iboutlet.isButton)
            continue;
        
        [outputMResult appendFormat:@"- (IBAction)%@Pushed\n", iboutlet.name];
        [outputMResult appendString:@"{\n"];
        [outputMResult appendFormat:@"\tNSLog(@\"%@ pushed\");\n", iboutlet.name];
        [outputMResult appendString:@"}\n"];
        [outputMResult appendString:@"\n"];
    }
    
    _generateMTextView.string = outputMResult;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}


#pragma mark - Actions

- (IBAction)generateButtonClicked:(NSButton *)sender
{
    [self generateOutlets];
}

@end
