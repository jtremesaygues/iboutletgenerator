//
//  ADIBOutlet.h
//  IBOutletGenerator
//
//                 LICENCE PUBLIQUE RIEN À BRANLER
// Version 1, Mars 2009
//
// Copyright (C) 2012 Jonathan Tremesaygues <jonathan@access-dev.com>
//
// La copie et la distribution de copies exactes de cette licence sont
// autorisées, et toute modification est permise à condition de changer
// le nom de la licence.
//
// CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION
// DE LA LICENCE PUBLIQUE RIEN À BRANLER
//
// 0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.

#import <Foundation/Foundation.h>

@interface ADOutlet : NSObject
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, readonly) BOOL isButton;
@property (nonatomic, readonly) BOOL isLabel;
@property (nonatomic, readonly) BOOL isScrollView;


- (id)init;
- (id)initWithType:(NSString * const)type name:(NSString * const)name;

+ (id)outletWithType:(NSString * const)type name:(NSString * const)name;
@end
